# Change Log

All notable changes to this project are documented in this file. This project
adheres to [Semantic Versioning](https://semver.org)

## [Unreleased]

### Changed

- Output to `*ERROR-OUTPUT*` during compilation now gets suppressed
- Rename `RUN` to `RUN-ALL` to prevent conflicts when imported together with
  packages under test.
- Replaced `TEST-CASE-EXISTS-ERROR` and `TEST-SUITE-EXISTS-ERROR` with
  `TEST-CASE-EXISTS-WARNING` and `TEST-SUITE-EXISTS-WARNING`. The respective
  reader functions for the test case name and test suite name have been
  renamed from `TEST-CASE-ERROR-TEST-CASE-NAME` and `TEST-SUITE-ERROR-NAME`
  to `TEST-CASE-WARNING-NAME` and `TEST-SUITE-WARNING-NAME`.

## [3.0.0] 2017-02-27

### Removed

- `TEST-CASE-COMPILATION-ERROR-RESULT-ERROR` function

## [2.0.0] 2017-02-26

### Changed

- `ADD-REPORTER` and `REMOVE-REPORTER` where replaced by `REGISTER-REPORTER` and
  `DEREGISTER-REPORTER`. The new functions take a function of no arguments. It is
  called once for each execution of RUN and RUN-TEST-SUITE to create a fresh
  reporter for that run.

## [1.0.0] 2017-02-03

Initial release
