# Task List

- Capture \*STANDARD-OUTPUT\* and \*ERROR-OUTPUT\* during test execution and only
  report it on error (like pytest).
- Fix a bug that prevents WITH-TEST-SUITE from working properly when nested
- Write tests.
- Add a tutorial on how to implement a test reporter.
- Allow the ignore parameter to be a string that contains the reason.
- Measure compile duration and run duration of tests

# Ideas

- Does IN-TEST-SUITE (and some related macros) need to be a macro?
- If %BUILD-TEST-LAMBDA and %EXECUTE-TEST-FUNCTION where exported generic
  methods, classes derived from TEST-CASE could be handled differently by
  those methods. That would allow, for example, to write a TEST-CASE derived
  class for parameterized test cases.
