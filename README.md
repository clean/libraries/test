# Clean Test Library

The Clean Test library provides a test framework for Common Lisp code. Tests
written using this library are structured into test suites and test cases.

## Clean Project

This library is part of the [Clean Project](https://gitlab.com/clean),
the outlet for my NIH syndrome.

## License

This library was written by Philipp Matthias Schäfer
(philipp.matthias.schaefer@posteo.de) and is published under the GPL3 license.
See [LICENSE] for a copy of that license.
