;;;; Copyright (c) 2017, 2018, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(in-package :asdf-user)

#-asdf3.1
(error "Clean Test requires ASDF 3.1 or later. Please upgrade your ASDF.")

(defsystem :clean-test
  :description "Test Framework"
  :long-description #.(read-file-string "README.md")
  :author "Philipp Matthias Schäfer <philipp.matthias.schaefer>"
  :licence "GNU General Public License Version 3"
  :version "3.0.0"
  :class :package-inferred-system
  :depends-on (:clean-test/source/all)
  :in-order-to ((test-op (load-op :clean-test/tests/all)))
  :perform (test-op (o c) (progn
                            (load-system :clean-standard-out-test-reporter)
                            (symbol-call :clean-test :run-all))))

;;; Systems
(register-system-packages :clean-test/source/all
                          '(:clean-test/source/all))
(register-system-packages :clean-util '(:clean-util))

;;; Additional Systems for Tests
(register-system-packages :clean-test/tests/all
                          '(:clean-test/tests/all))
