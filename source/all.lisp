;;;; Copyright (c) 2017, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(uiop:define-package :clean-test/source/all
  (:nicknames :clean-test)
  (:use-reexport
   :clean-test/source/condition
   :clean-test/source/definition/all
   :clean-test/source/failure
   :clean-test/source/assert
   :clean-test/source/result
   :clean-test/source/reporting/all
   :clean-test/source/execution))
