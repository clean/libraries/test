;;;; Copyright (c) 2017, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(defpackage :clean-test/source/assert
  (:use :common-lisp
        :clean-test/source/failure)
  (:export :assert-error
           :assert-error-failure-condition
           :assert-error-failure-condition-error-class
           :assert-failure-condition
           :assert-failure-condition-test-form
           :assert-that
           :assert-that-failure-condition))

(in-package :clean-test/source/assert)

(define-condition assert-failure-condition (failure-condition)
  ((%test-form :initarg :test-form
               :reader assert-failure-condition-test-form))
  (:documentation "Base condition for assertion failure conditions.

ASSERT-FAILURE-CONDITION-TEST-FORM returns the form that defined the failed
assertion."))

(define-condition assert-that-failure-condition (assert-failure-condition) ()
  (:documentation "An ASSERT-THAT condition failed."))

(defmacro assert-that (test-form)
  "Assert that TEST-FORM returns a true value.

Errors:

ASSERT-THAT-FAILURE-CONDITION if TEST-FORM returns NIL."
  `(unless ,test-form
     (error 'assert-that-failure-condition :test-form ',test-form)))

(define-condition assert-error-failure-condition (assert-failure-condition)
  ((%error-class :initarg :error-class
                 :reader assert-error-failure-condition-error-class))
  (:documentation "An ASSERT-ERROR condition failed.

ASSERT-ERROR-FAILURE-CONDITION-ERROR-CLASS returns the class of the expected
error."))

(defmacro assert-error ((error-class) test-form)
  "Assert that TEST-FORM signals a condition of type ERROR-CLASS.

Errors:

ASSERT-ERROR-FAILURE-CONDITION if TEST-FORM does not signal a condition of type
ERROR-CLASS."
  `(handler-case
       (progn
         ,test-form
         (error 'assert-error-failure-condition
                :error-class ',error-class
                :test-form ',test-form))
     (,error-class ())))
