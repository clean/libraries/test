;;;; Copyright (c) 2017, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(defpackage :clean-test/source/definition/test-case
  (:use :common-lisp
        :clean-test/source/condition
        :clean-test/source/definition/test-suite/all-internal
        :clean-util)
  (:export :define-test-case
           :test-case
           :test-case-body
           :test-case-condition
           :test-case-condition-name
           :test-case-description
           :test-case-exists-warning
           :test-case-exists-warning-test-suite-name
           :test-case-ignore
           :test-case-name
           :test-case-warning))

(in-package :clean-test/source/definition/test-case)

(defclass test-case ()
  ((%name :initarg :name
          :reader test-case-name)
   (%description :initarg :description
                 :reader test-case-description)
   (%ignore :initarg :ignore
            :reader test-case-ignore)
   (%body :initarg :body
          :reader test-case-body))
  (:documentation
   "A TEST-CASE object holds forms that are independently compiled and executed
during test runs.

TEST-CASE-NAME returns the name, a string, of a test case.

TEST-CASE-DESCRIPTION returns the description, a string, of a test case.

TEST-CASE-IGNORED returns a generalized boolean indicating whether the test case
will be run during execution. For ignored test cases a TEST-CASE-IGNORED
result object is created during execution.

TEST-CASE-BODY returns a list of forms that constitute the body of the test
case.

The TEST-CASE class is not designed to be instantiated by client code. Use
DEFINE-TEST-CASE instead."))

(define-condition test-case-warning (clean-test-warning)
  ((name :initarg :name
         :reader test-case-warning-name))
  (:documentation "Base condition for test case related conditions.

TEST-CASE-WARNING-NAME returns the name of the test case related to this
condition"))

(define-condition test-case-exists-warning (test-case-warning)
  ((test-suite-name :initarg :test-suite-name
                    :reader test-case-exists-warning-test-suite-name))
  (:report (lambda (condition stream)
             (format stream
                     "A test-case \"~A\" already exists in test suite \"~A\""
                     (test-case-warning-name condition)
                     (test-case-exists-warning-test-suite-name condition))))
  (:documentation
   "Indicates that a test case of a given name already exists in the given test
suite.

For this condition TEST-CASE-WARNING-NAME returns the name of the to be defined
test case.

TEST-CASE-EXISTS-WARNING-TEST-SUITE-NAME returns the name of the test suite in
which the test case was to be defined."))

(defun %add-test-case (name test-suite-name class-name class-args)
  (let ((test-suite (%get-test-suite test-suite-name
                                     :error-on-missing t
                                     :use-default t)))
    (when (gethash name (test-suite-cases test-suite))
      (warn 'test-case-exists-warning
             :test-case-name name
             :test-suite-name test-suite-name))
    (let ((test-case (apply #'make-instance class-name :name name class-args)))
      (setf (gethash name (test-suite-cases test-suite)) test-case))))

(defmacro define-test-case ((name &key test-suite ignore (class 'test-case))
                            &body body)
  "Defines a test case.

Parameters:

NAME (STRING-DESIGNATOR)
TEST-SUITE ((OR STRING-DESIGNATOR NIL))
IGNORE (T)
CLASS ((OR SYMBOL LIST))
BODY (FORMS)

Description:

`(STRING NAME)` is the test cases name.

If TEST-SUITE is not NIL, the test case parent will be the test suite named
`(STRING TEST-SUITE)`. Otherwise the default test suite will be used. See
WITH-TEST-SUITE and IN-TEST-SUITE on how to set the default test suite.

IGNORE is interpreted as a generalized boolean. If it is true, the test case
will not be run during execution. Instead a A TEST-CASE-IGNORED object will be
returned as as its result.

If CLASS is a non-NIL symbol, it must name a class derived from TEST-CASE. It
will be instantiated instead of TEST-CASE. If CLASS is a list, the first element
must name a class derived from-end TEST-CASE. It will be instantiated instead of
TEST-CASE. The remaining elements of LIST will be used as additional keyword
arguments to MAKE-INSTANCE.

BODY will be stored in the test case. During execution it will be compiled and
executed wrapped in a lambda. Compilation errors of BODY lead to a
TEST-CASE-COMPILATION-ERROR-RESULT result. Errors during execution of BODY lead
to a TEST-CASE-ERROR-RESULT result. Signaling a FAILURE-CONDITION during
execution of BODY leads to a TEST-CASE-FAILURE-RESULT result.

If BODY starts with a string, that string will be used as the description of the
test case.

Functions are lexically bound to symbols IGNORE-TEST-CASE and FAIL-TEST-CASE.
Calling IGNORE-TEST-CASE results in the test case being handled as if it were
declared ignored. FAIL-TEST-CASE signals a FORCED-FAILURE-CONDITION.

Errors:

TEST-CASE-EXISTS-ERROR if a test case of the given name already exists in the
parent test suite.
UNKNOWN-TEST-SUITE-ERROR if TEST-SUITE is non-NIL and no test suite of that name
exists.
NO-DEFAULT-TEST-SUITE-ERROR if TEST-SUITE is NIL and no default test suite is
set."
  (check-type class (or symbol list))
  (let* ((description (when (stringp (car body)) (car body)))
         (body (if description (cdr body) body))
         (class-args (list* :ignore ignore
                            :description description
                            :body body
                            (when (consp class) (cdr class))))
         (class (if (consp class) (car class) class)))
    `(%add-test-case ,(string name) ,(and test-suite (string test-suite))
                     ',class ',class-args)))
