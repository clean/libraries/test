;;;; Copyright (c) 2017, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(defpackage :clean-test/source/definition/test-suite/api-internal
  (:use :common-lisp
        :clean-test/source/definition/test-suite/condition
        :clean-test/source/definition/test-suite/model
        :clean-util)
  (:export :delete-test-suite

           :%get-root-test-suites
           :%get-test-suite
           :%make-test-suite
           :*%test-suite*))

(in-package :clean-test/source/definition/test-suite/api-internal)

(defvar *%test-suite* nil)

(let ((all-test-suites (make-hash-table :test 'equal))
      (root-test-suites (make-hash-table :test 'equal)))

  (defun %get-root-test-suites ()
    (hash-table-values root-test-suites))

  (defun %get-test-suite (name &key error-on-missing use-default)
    (let-return (test-suite (or (gethash name all-test-suites)
                                (and use-default *%test-suite*)))
      (when (and error-on-missing (not test-suite))
        (if (and (not name) use-default)
            (error 'no-default-test-suite-error)
            (error 'unknown-test-suite-error :name name)))))

  (defun %get-parent-container (test-suite)
    (let ((parent (test-suite-parent test-suite)))
      (if parent (test-suite-children parent) root-test-suites)))

  (defmethod initialize-instance :after ((test-suite test-suite) &key)
    (let ((name (test-suite-name test-suite))
          (parent-container (%get-parent-container test-suite)))
      (dolist (container (list all-test-suites parent-container))
        (setf (gethash name container) test-suite))))

  (defun delete-test-suite (name)
    "Delete test suite

Parameter:

NAME (STRING-DESIGNATOR)

Description:

Deletes the test suite named `(STRING NAME)` if it exists."
    (let* ((name (string name))
           (test-suite (%get-test-suite name :error-on-missing t))
           (parent-container (%get-parent-container test-suite)))
      (dolist (container (list all-test-suites parent-container))
        (remhash name container)))))

(defun %make-test-suite (name parent-name description)
  (make-instance 'test-suite
                 :name name
                 :description description
                 :parent (when parent-name
                           (%get-test-suite parent-name :error-on-missing t))))

(defmethod make-instance :before ((class (eql (find-class 'test-suite)))
                                  &key name &allow-other-keys)
  (when (%get-test-suite name)
    (warn 'test-suite-exists-warning :name name)))
