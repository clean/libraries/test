;;;; Copyright (c) 2017, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(defpackage :clean-test/source/definition/test-suite/api
  (:use :common-lisp
        :clean-test/source/definition/test-suite/model
        :clean-test/source/definition/test-suite/api-internal
        :clean-util)
  (:export :define-test-suite
           :delete-test-suite
           :in-test-suite
           :with-test-suite))

(in-package :clean-test/source/definition/test-suite/api)

(defmacro define-test-suite ((name &key parent) &optional description)
  "Define a test suite.

Parameters:

NAME (STRING-DESIGNATOR)
PARENT ((OR NULL STRING-DESIGNATOR))
DESCRIPTION ((OR NULL STRING))

Description:

`(STRING NAME)` is the test suite's name.

If PARENT is not NIL, `(STRING PARENT)` names the new test suite's parent.

If DESCRIPTION is not NIL, it will be used as the description of the test suite.

Errors:

INVALID-TEST-SUITE-NAME-ERROR if NAME gets coerced to the empty string.
TEST-SUITE-EXISTS-ERROR if a test suite named NAME already exists.
UNKOWN-TEST-SUITE-ERROR if PARENT is not NIL and no test suite named PARENT
exists."
  (check-type description (or null string))
  (unless name
    (error 'invalid-test-suite-name-error :name name))
  `(%make-test-suite ,(string name)
                     ,(and parent (string parent))
                     ,description))

(defmacro in-test-suite (name)
  "Set the default test suite globally.

Parameters:

NAME (STRING-DESIGNATOR)

Description:

Set the global default test suite to the one designated by NAME.

Errors:

UNKNOWN-TEST-SUITE-ERROR if no test suite named NAME exists."
  `(let* ((name ,(string name))
          (test-suite (%get-test-suite name :error-on-missing t)))
     (setf *%test-suite* test-suite)))

(defmacro with-test-suite ((name &key parent) &body body)
  "Defines a new test suite and makes it the default test suite in its body's
dynamic scope.

Parameters:

NAME (STRING-DESIGNATOR)
PARENT ((OR NULL STRING-DESIGNATOR))
BODY (FORMS)

Description:

`(STRING NAME)` is the test suite's name.

If PARENT is not NIL, `(STRING PARENT)` names the new test suite's parent.

The newly created test suite is the default test suite in the dynamic
environment of BODY.

If BODY starts with a string, that string will be used as the description of the
new test suite.

Errors:

INVALID-TEST-SUITE-NAME-ERROR if NAME gets coerced to the empty string.
TEST-SUITE-EXISTS-ERROR if a test suite named NAME already exists.
UNKOWN-TEST-SUITE-ERROR if PARENT is not NIL and no test suite named PARENT
exists."
  (unless name
    (error 'invalid-test-suite-name-error :name name))
  (let ((body (if (stringp (car body)) (cdr body) body))
        (description (when (stringp (car body)) (car body))))
    `(let ((*%test-suite* (%make-test-suite ,(string name)
                                            ,(and parent (string parent))
                                            ,description)))
       ,@body)))
