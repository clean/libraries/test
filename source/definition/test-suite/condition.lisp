;;;; Copyright (c) 2017, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(defpackage :clean-test/source/definition/test-suite/condition
  (:use :clean-test/source/condition
        :common-lisp)
  (:export :invalid-test-suite-name-error
           :no-default-test-suite-error
           :test-suite-condition
           :test-suite-condition-name
           :test-suite-error
           :test-suite-exists-warning
           :unknown-test-suite-error))

(in-package :clean-test/source/definition/test-suite/condition)

(define-condition no-default-test-suite-error (clean-test-error) ()
  (:documentation
   "No default test suite was set, when that was expected.

Whenever a test suite is expected, no test suite was provided by argument, and
no default test suite was set, NO-DEFAULT-TEST-SUITE-ERROR is signaled. This
only affects the macro DEFINE-TEST-CASE.

See WITH-TEST-SUITE and IN-TEST-SUITE on how to set the default test suite.")
  (:report (lambda (condition stream)
             (declare (ignore condition))
             (format stream "No default test suite set"))))

(define-condition test-suite-condition ()
  ((name :initarg :name
         :reader test-suite-condition-name))
  (:documentation
   "Base condition for conditions that are related to a specific test suite.

TEST-SUITE-CONDITION-NAME returns the name of the concerned test suite."))

(define-condition test-suite-error (clean-test-error test-suite-condition) ()
  (:documentation
   "Base condition for error conditions that are related to a specific test
suite."))

(define-condition invalid-test-suite-name-error (test-suite-error) ()
  (:documentation
   "The argument provided for a test suite name parameter is invalid.

Any STRING-DESIGNATOR that is not coerced to the empty string by STRING is
a valid argument for test suite name parameters.")
  (:report (lambda (condition stream)
             (format stream "\"~A\" is not a valid test suite name"
                     (test-suite-condition-name condition)))))

(define-condition unknown-test-suite-error (test-suite-error) ()
  (:documentation
   "The argument provided for a test suite name parameter does not designate
a test suite.")
  (:report (lambda (condition stream)
             (format stream "No test-suite named \"~A\" exists"
                     (test-suite-condition-name condition)))))

(define-condition test-suite-exists-warning
    (clean-test-warning test-suite-condition) ()
  (:documentation
   "A test suite of the proposed name for a new test suite already exists.")
  (:report (lambda (condition stream)
             (format stream "A test-suite named \"~A\" already exists"
                     (test-suite-condition-name condition)))))
