;;;; Copyright (c) 2017, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(defpackage :clean-test/source/definition/test-suite/model
  (:use :common-lisp)
  (:export :test-suite
           :test-suite-cases
           :test-suite-children
           :test-suite-description
           :test-suite-name
           :test-suite-parent))

(in-package :clean-test/source/definition/test-suite/model)

(defclass test-suite ()
  ((%name :initarg :name
          :reader test-suite-name)
   (%description :initarg :description
                 :reader test-suite-description)
   (%parent :initarg :parent
            :reader test-suite-parent)
   (%children :initform (make-hash-table :test 'equal)
              :reader test-suite-children)
   (%cases :initform (make-hash-table :test 'equal)
           :reader test-suite-cases))
  (:documentation
   "A TEST-SUITE object is a collection test cases and child test suites.

TEST-SUITE-NAME returns the name, a string, of a test suite.

TEST-SUITE-DESCRIPTION returns the description, a string, of a test suite.

TEST-SUITE-PARENT returns the parent test suite of a test suite or NIL, if it
has none. A suite without a parent is a root test suite.

TEST-SUITE-CHILDREN returns the child test suites of a test suite. They are
returned as a hash table using test suite names as keys.

TEST-SUITE-CASES returns the test cases of a test suite. They are returned as a
hash table using test case names as keys.

The TEST-SUITE class is not designed to be instantiated by client code. Use
DEFINE-TEST-SUITE or WITH-TEST-SUITE instead."))
