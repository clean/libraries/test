;;;; Copyright (c) 2017, 2019, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(defpackage :clean-test/source/execution
  (:use :common-lisp
        :clean-test/source/failure
        :clean-test/source/assert
        :clean-test/source/definition/all-internal
        :clean-test/source/reporting/all-internal
        :clean-test/source/result
        :clean-util)
  (:export :fail-test-case
           :forced-failure-condition
           :ignore-test-case
           :run-all
           :run-test-suite))

(in-package :clean-test/source/execution)

(define-condition %ignore-test-case-condition (condition) ())

(define-condition forced-failure-condition (failure-condition) ()
  (:documentation
   "Signaled by FAIL-TEST-CASE in test case bodies

Description:

If FAIL-TEST-CASE is called in a test case, FORCED-FAILURE-CONDITION is
signaled. A TEST-CASE-FAILURE-RESULT result is created for the test case."))

(defun %build-test-lambda (test-case)
  `(lambda ()
     (flet ((ignore-test-case ()
              (signal '%ignore-test-case-condition))
            (fail-test-case ()
              (error 'forced-failure-condition)))
       ;; Otherwise SBCL will complain about unused functions. As soon as I get
       ;; around to writing a proper code walker, we can get rid of this.
       (unless (%get-root-test-suites)
         (ignore-test-case)
         (fail-test-case))
       ,@(test-case-body test-case))))

(defun %execute-test-function (test-case function)
  (declare (ignore test-case))
  (funcall function))

(defun %truly-run-test-case (test-case)
  (let ((*error-output* (make-string-output-stream)))
    (multiple-value-bind (function ignored failure-p)
        (with-compilation-unit (:override t)
          (compile nil (%build-test-lambda test-case)))
      (declare (ignore ignored))
      (if failure-p
          (make-instance 'test-case-compilation-error-result
                         :test-case test-case)
          (handler-case
              (progn
                (%execute-test-function test-case function)
                (make-instance 'test-case-success-result :test-case test-case))
            (%ignore-test-case-condition ()
              (make-instance 'test-case-ignored-result :test-case test-case))
            (failure-condition (condition)
              (make-instance 'test-case-failure-result
                             :test-case test-case
                             :condition condition))
            (error (error)
              (make-instance 'test-case-error-result
                             :test-case test-case
                             :error error)))))))

(defun %run-test-case (test-case test-suite)
  (%signal-on-test-case-start test-case test-suite)
  (let-return (result
               (if (test-case-ignore test-case)
                   (make-instance 'test-case-ignored-result
                                  :test-case test-case)
                   (%truly-run-test-case test-case)))
    (%signal-on-test-case-end result test-suite)))

(defun %run-test-suite (test-suite)
  (%signal-on-test-suite-start test-suite)
  (let ((results (mapvalues (rcurry #'%run-test-case test-suite)
                            (test-suite-cases test-suite)))
        (child-results (mapvalues #'%run-test-suite
                                  (test-suite-children test-suite))))
    (let-return (result (make-instance 'test-suite-result
                                       :test-suite test-suite
                                       :child-results child-results
                                       :results results))
      (%signal-on-test-suite-end result))))

(defun %run-test-suites (test-suites)
  (%with-reporters
    (%signal-on-start test-suites)
    (let-return (results (mapcar #'%run-test-suite test-suites))
      (%signal-on-end results))))

(defun run-test-suite (name)
  "Run a single test suite and return the result

Parameters:

NAME (STRING-DESIGNATOR)

Description:

Coerces NAME to a string, runs the test suite of that name and all its
descendants, depth first, and then returns the a single TEST-SUITE-RESULT
object.

Errors:

UNKOWN-TEST-SUITE-ERROR if no test suite of the given name exists."
  (car (%run-test-suites (list (%get-test-suite (string name)
                                                :error-on-missing t)))))

(defun run-all ()
  "Run all test suites and return the results

Description:

Runs all root test suites and their descendants, depth first, then returns a
list of TEST-SUITE-RESULT objects, one for each root test suite."
  (%run-test-suites (%get-root-test-suites)))
