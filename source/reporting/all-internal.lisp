;;;; Copyright (c) 2017, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.

(defpackage :clean-test/source/reporting/all-internal
  (:use :common-lisp
        :clean-util
        :clean-test/source/definition/all-internal
        :clean-test/source/result)
  (:export :deregister-reporter
           :on-end
           :on-start
           :on-test-case-end
           :on-test-case-start
           :on-test-suite-end
           :on-test-suite-start
           :register-reporter

           :%signal-on-end
           :%signal-on-start
           :%signal-on-test-case-end
           :%signal-on-test-case-start
           :%signal-on-test-suite-end
           :%signal-on-test-suite-start
           :%with-reporters))

(in-package :clean-test/source/reporting/all-internal)

(defvar *%constructors* (make-hash-table))

(defvar *%reporters*)

(defun register-reporter (constructor)
  "Register reporter constructor function CONSTRUCTOR.

Parameters:

CONSTRUCTOR ((FUNCTION () T))

Description:

Register reporter constructor function CONSTRUCTOR. For each execution of RUN or
RUN-TEST-SUITE a set of reporters is created by calling each registered reporter
constructor once. Return T if CONSTRUCTOR was already registered, NIL
otherwise."
  (check-type constructor function)
  (prog1 (gethash constructor *%constructors*)
    (setf (gethash constructor *%constructors*) t)))

(defun deregister-reporter (constructor)
  "Deregister reporter constructor function CONSTRUCTOR.

Parameters:

CONSTRUCTOR ((FUNCTION () T))

Description:

Deregister reporter constructor function CONSTRUCTOR. Return T if CONSTRUCTOR
was registered, NIL otherwise."
  (check-type constructor function)
  (remhash constructor *%constructors*))

(defmacro %with-reporters (&body body)
  `(let ((*%reporters* (mapkeys #'funcall *%constructors*)))
     ,@body))

(macrolet ((defhook (name (&rest args) documentation)
             `(progn
                (defgeneric ,name (reporter ,@args)
                  (:documentation ,documentation)
                  (:method (reporter ,@args)
                    (declare (ignore reporter ,@args))
                    (values)))
                (defun ,(symbolicate '%signal- name) (,@args)
                  (dolist (reporter *%reporters*)
                    (,name reporter ,@args)))
                    (values))))
  (defhook on-test-case-start (test-case test-suite)
    "Called once for every registered reporter, before TEST-CASE of TEST-SUITE
is run.")
  (defhook on-test-case-end (test-case-result test-suite)
    "Called once for every registered reporter, after the test case belonging to
TEST-SUITE and corresponding to TEST-CASE-RESULT is run.")
  (defhook on-test-suite-start (test-suite)
    "Called once for every registered reporter, before TEST-SUITE is run.")
  (defhook on-test-suite-end (test-suite-result)
    "Called once for every registered reporter, after the test test-suite
corresponding to TEST-SUITE-RESULT is run.")
  (defhook on-start (test-suites)
    "Called once for every registered reporter, whenever RUN or RUN-TEST-SUITE
is called before any test suite is run.

TEST-SUITES is a list of test suites in the order they are run.")
  (defhook on-end (test-suite-results)
    "Called once for every registered reporter, whenever RUN or RUN-TEST-SUITE
is called after all test suites are run.

TEST-SUITE-RESULTS is a list of test suite results in the order corresponding
test suites where run."))
