;;;; Copyright (c) 2017, Philipp Matthias Schäfer
;;;; (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Test library.
;;;;
;;;; The Clean Test library is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; The Clean Test library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;;;; Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Test library. If not, see <https://www.gnu.org/licenses/>.


(defpackage :clean-test/source/result
  (:use :common-lisp)
  (:export :test-case-compilation-error-result
           :test-case-error-result
           :test-case-error-result-error
           :test-case-ignored-result
           :test-case-failure-result
           :test-case-failure-result-condition
           :test-case-result
           :test-case-result-test-case
           :test-case-success-result
           :test-suite-result
           :test-suite-result-child-results
           :test-suite-result-test-case-results
           :test-suite-result-test-suite))

(in-package :clean-test/source/result)

(defclass test-suite-result ()
  ((test-suite :initarg :test-suite
               :reader test-suite-result-test-suite)
   (child-results :initarg :child-results
                  :reader test-suite-result-child-results)
   (test-case-results :initarg :results
                      :reader test-suite-result-test-case-results))
  (:documentation "Result of running a test suite.

The result was created for the test suite returned by
TEST-SUITE-RESULT-TEST-SUITE.

TEST-SUITE-RESULT-CHILD-RESULTS returns a list of test suite results for the
children of the test suite.

TEST-SUITE-RESULT-TEST-CASE-RESULTS returns a list of test case results for
the test cases of the test suite."))

(defclass test-case-result ()
  ((test-case :initarg :test-case
              :reader test-case-result-test-case))
  (:documentation "Base class for test case result classes.

An object represents the result of running the test case returned by
TEST-CASE-RESULT-TEST-CASE."))

(defclass test-case-success-result (test-case-result) ()
  (:documentation "Test case result for successfully executed test cases."))

(defclass test-case-ignored-result (test-case-result) ()
  (:documentation "Test case result for ignored test cases."))

(defclass test-case-failure-result (test-case-result)
  ((condition :initarg :condition
              :reader test-case-failure-result-condition))
  (:documentation "Test case result for failed test cases.

TEST-CASE-FAILURE-RESULT-CONDITION returns the FAILURE-CONDITION that caused the
test case's failure."))

(defclass test-case-error-result (test-case-result)
  ((error :initarg :error
          :reader test-case-error-result-error))
  (:documentation
   "Test case result for test cases whose execution signals an unexpected error.

TEST-CASE-ERROR-RESULT-ERROR returns the error that was unexpectedly
signaled."))

(defclass test-case-compilation-error-result (test-case-result) ()
  (:documentation
   "Test case result for test cases that do not compile successfully."))
